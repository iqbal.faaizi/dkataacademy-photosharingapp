/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  StatusBar,
} from 'react-native';
import MainNavigator from './MainNavigator'
import { Provider } from 'react-redux'
import Store from './src/Store'

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor="#c4a300" />
      <Provider store={Store}>
        <MainNavigator />
      </Provider>
    </>
  );
};

export default App;
