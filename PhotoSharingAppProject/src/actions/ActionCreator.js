import axios from 'axios'
import {
    getDataSuccess, 
    postDataSuccess, 
    putDataSuccess, 
    getUserDataSuccess, 
    authSuccess, 
    dataError
} from './Action'

export const getData = url => {
    return dispatch => {
      axios
        .get(url)
        .then(response => {
          dispatch(getDataSuccess(response.data));
        })
        .catch(error => {
          dispatch(dataError(error));
        });
    };
};

export const getUserData = url => {
    return dispatch => {
      axios
        .get(url)
        .then(response => {
          dispatch(getUserDataSuccess(response.data));
        })
        .catch(error => {
          dispatch(dataError(error));
        });
    };
};

export const postData = (url, payload) => {
    return dispatch => {
        axios.post(url, payload)
        .then(response => {
            dispatch(postDataSuccess(response.data))
        })
        .catch(error => {
            dispatch(dataError(error))
        })
    }
}

export const putData = (url, payload) => {
    return dispatch => {
        axios
        .put(url, payload)
        .then(response => {
            dispatch(putDataSuccess(response))
        })
        .catch(error => {
            dispatch(dataError(error))
        })
    }
}

export const authEmail = (email) => {
    return dispatch => {
        dispatch(authSuccess(email))
    }
}