import React from 'react'
import {Text, View, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/Feather';
import { Input } from 'react-native-elements';
import md5 from 'md5';
import axios from 'axios';
import {regStyles } from '../assets/styles/appStyles'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { authEmail } from '../actions/ActionCreator'

class Login extends React.Component {
    static navigationOptions = { header: null }

    constructor (props) {
        super(props)
        this.state = {
            data: {
                email: '',
                password: '',
            },
        }
    }

    async componentDidMount() { 
        const checkAuth = await AsyncStorage.getItem('email')
        if(checkAuth) {
            this.props.navigation.navigate('Content')
        } else {
            this.props.navigation.navigate('Login')
        }
    }

    _onPressLogin = async() => {
        const { data } = this.state
        const { navigate } = this.props.navigation
        const payload = {
            emailID: data.email,
            password: md5(data.password)
        }
        let email = data.email
        try {
            let response = await axios.post(`http://10.0.3.2:3000/user/login`, payload)
            let responseJson = await response.data
            if (responseJson.auth == true) {
                //Set async storage
                AsyncStorage.setItem('email', email)
                this.props.authEmail(data.email)

                alert('Successfully logged in')
                setTimeout( ()=> {
                    navigate('Content')
                },1000 );

            } else {
                alert('Input correct email or password!')
            }
        } catch(err) {
            console.error(err)
        }
    }

    render() {
        //console.log(this.state)
        return (
        <>
            <View style={regStyles.container}>
                <View style={regStyles.inputGroup}>
                    <Text style={regStyles.regHeader}>LOGIN</Text>

                    <Input
                        containerStyle={{marginBottom: 20}}
                        inputContainerStyle={{marginHorizontal: 10}}
                        placeholder='Email ID'
                        keyboardType='email-address'
                        leftIcon={<Icon name='mail' size={24} color='black' style={{left: -13}} />}
                        onChangeText={(email) => (
                            this.setState({...this.state,
                                data : {...this.state.data, email: email}
                            })
                        )}
                        value={this.state.data.email}
                        autoCapitalize='none'
                    />

                    <Input
                        containerStyle={{marginBottom: 50}}
                        inputContainerStyle={{marginHorizontal: 10}}
                        placeholder='Password'
                        secureTextEntry={true}
                        leftIcon={<Icon name='lock' size={24} color='black' style={{left: -13}} />}
                        onChangeText={(password) => (
                            this.setState({...this.state,
                                data : { ...this.state.data, password: password }
                            })
                        )}
                        value={this.state.data.password}
                        autoCapitalize='none'
                    />
                    
                    <TouchableOpacity  
                        style={regStyles.btnSubmit}
                        onPress={() => this._onPressLogin()}
                    >
                            <Text>SIGN IN</Text>
                    </TouchableOpacity>

                </View>
            </View>

            <View style={{backgroundColor:'#000'}}>
                <TouchableOpacity  style={regStyles.loginFooter}
                    onPress={() => this.props.navigation.navigate('Register')}
                >
                    <Text style={regStyles.loginTxt}>Don't have an account? Register here</Text>
                </TouchableOpacity>
            </View>
        </>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        authEmail: (email) => {
            dispatch(authEmail(email));
        }
    };
};

const mapStateToProps = state => ({
    email: state.apiReducer.email
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);
