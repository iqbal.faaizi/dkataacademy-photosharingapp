import React, {Component} from 'react';
import { 
    View, Text, 
    TouchableOpacity, 
    Keyboard, 
    Image,
    ScrollView,
    ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Input } from 'react-native-elements';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import axios from 'axios';
import {profStyles} from '../assets/styles/appStyles'
import PhotoUpload from 'react-native-photo-upload'

export default class Profile extends Component {
    static navigationOptions = {header: null}
    
    constructor(props) {
        super(props);
        const { getParam } = this.props.navigation
        this.state={
            emailID: getParam('emailID'),
            userData: {
                fullname : '',
                username:'',
                bio: '',
                address: '',
                phone: 0,
                gender : '',
                born_date: '',
            },
            dateStr:'',
            date: new Date(),
            mode: 'date',
            show: false,
            photoData: '',
        }
    }

    setDate = (event, date) => {
        date = date || this.state.date;
        this.setState({
            show: Platform.OS === 'ios' ? true : false,
            userData: {...this.state.userData, born_date: date},
            dateStr: moment(date).format("YYYY-MMM-DD")
        });
    }

    show = mode => {
        this.setState({
            show: true,
            mode,
        });
    }

    datepicker = () => {
        this.show('date');
    }

    _onPressSave = async() => {
        const {userData, emailID, photoData} = this.state
        let data = {
            userData: userData,
            photoData: photoData
        }
        
        const response = await axios.put(`http://192.168.56.1:3000/updProfile/${emailID}`, data)

        const responseJson = response.data
        if(responseJson) {
            alert("Your personal data has successfully saved, please login using your account!")
            setTimeout( ()=> {
                this.props.navigation.navigate('Login')
            },1000 );
        } else {
            alert("Failed saving data. Please check again!")
        }
    }

    render() {
        const placeholder = {
            label: 'Select a gender...',
            value: null,
            color: '#9EA0A4',
        };
        const { show, date, mode } = this.state;

        return (
            <View style={{flex: 1}}>
                {/* Header */}
                <View style={profStyles.header}>
                    <View style={{flex:1, justifyContent: 'center', marginLeft: 20}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>Edit Profile</Text>
                    </View>
                    
                    <TouchableOpacity style={{flex: 1, justifyContent:'center', marginRight:20 }}
                        onPress={() => this._onPressSave()}
                    >
                        <Text style={{textAlign: 'right',fontSize: 18}}>Save</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView>
                <View style={{backgroundColor: '#e2e2e2', flex: 1}}>
                    <View style={{backgroundColor:'#fff', marginTop: 20}}>       

                        {/* Image picker */}
                        <PhotoUpload
                            onPhotoSelect={avatar => {
                                if (avatar) {
                                    this.setState({ photoData: avatar })
                                }
                            }}
                        >
                            <Image
                                style={profStyles.imgContainer}
                                resizeMode='cover'
                                source={require('../../public/assets/images/dp_null.png')}
                            />
                        </PhotoUpload>

                        <Input 
                            containerStyle={{marginBottom: 20}}
                            inputContainerStyle={{marginHorizontal: 10}}
                            placeholder='Full Name'
                            leftIcon={<Icon name='credit-card' size={24} color='black' style={{left: -13, width:30}} />}
                            onChangeText={text => this.setState({
                                userData: {...this.state.userData, fullname: text}
                            })}
                        />
                        <Input 
                            containerStyle={{marginBottom: 20}}
                            inputContainerStyle={{marginHorizontal: 10}}
                            placeholder='Username'
                            leftIcon={<Icon name='user-tie' size={24} color='black' style={{left: -13, width:30}}/> }
                            onChangeText={text => this.setState({
                                userData: {...this.state.userData, username: text}
                            })}
                        />
                        <Input 
                            containerStyle={{marginBottom: 20}}
                            inputContainerStyle={{marginHorizontal: 10}}
                            placeholder='Bio'
                            leftIcon={<Icon name='info-circle' size={24} color='black' style={{left: -13, width:30}} />}
                            onChangeText={text => this.setState({
                                userData: {...this.state.userData, bio: text}
                            })}
                        />
                        <Input 
                            containerStyle={{marginBottom: 20}}
                            inputContainerStyle={{marginHorizontal: 10}}
                            placeholder='Home Address'
                            leftIcon={<Icon name='home' size={24} color='black' style={{left: -13, width:30}} />}
                            onChangeText={text => this.setState({
                                userData: {...this.state.userData, address: text}
                            })}
                        />
                        
                        <Input 
                            containerStyle={{marginBottom: 20}}
                            inputContainerStyle={{marginHorizontal: 10}}
                            placeholder='Phone'
                            keyboardType='phone-pad'
                            leftIcon={<Icon name='phone' size={24} color='black' style={{left: -13, width:30}} />}
                            onChangeText={num => this.setState({
                                userData: {...this.state.userData, phone: num}
                            })}
                        />

                        <View style={{flexDirection: 'row', marginBottom: 20}}>
                            <View style={{width:40, justifyContent: 'center', marginLeft: 20}}>
                                <Icon name='venus-mars' size={24} color='black'/>
                            </View>
                            <View style={{flex: 5}}>
                                <RNPickerSelect
                                    placeholder={placeholder}
                                    onValueChange={value => this.setState({
                                        userData: {...this.state.userData, gender: value}
                                    })}
                                    items={[
                                        { label: 'Male', value: 'male' },
                                        { label: 'Female', value: 'female' },
                                    ]}
                                />
                            </View>
                        </View>

                        <Input 
                            containerStyle={{marginBottom: 20}}
                            inputContainerStyle={{marginHorizontal: 10}}
                            placeholder='Born Date'
                            leftIcon={<Icon name='calendar-week' size={24} color='black' style={{left: -13, width:30}} />}
                            onTouchEnd={this.datepicker}
                            onFocus={Keyboard.dismiss}
                            value={this.state.dateStr}
                        />
                        { show && <DateTimePicker value={date}
                            mode={mode}
                            is24Hour={true}
                            display="default"
                            onChange={this.setDate} />
                        }
                    </View>
                </View>
                </ScrollView>
            </View>
        )
    }
}