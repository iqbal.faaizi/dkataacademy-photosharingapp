import React, { Component } from 'react';
import { 
    View, 
    Text,
    TouchableOpacity
} from 'react-native';
import { homeStyles } from '../assets/styles/appStyles'
import AsyncStorage from '@react-native-community/async-storage'

export default class HomePage extends Component {
    static navigationOptions = { header: null }
    
    async componentDidMount() { 
        const checkAuth = await AsyncStorage.getItem('email')
        if(checkAuth) {
            this.props.navigation.navigate('Content')
        } else {
            this.props.navigation.navigate('HomePage')
        }
    }

    render () {
        const { navigate } = this.props.navigation
        return (
        <>
            <View style={homeStyles.topContainer}>
                <Text>---APP LOGO---</Text>
            </View>

            <View style={homeStyles.btmContainer}>
                <TouchableOpacity style={homeStyles.btnContainer}
                    onPress={() => navigate('Login')}
                >
                    <Text style={homeStyles.btnTxt}>LOGIN</Text>
                </TouchableOpacity>

                <TouchableOpacity style={homeStyles.btnContainer}
                    onPress={() => navigate('Register')}
                >
                    <Text style={homeStyles.btnTxt}>REGISTER</Text>
                </TouchableOpacity>
            </View>
        </>
        )
    }
}