import React, { Component } from 'react';
import { 
    View, 
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { validate } from 'validate.js';
import Icon from 'react-native-vector-icons/Feather';
import { Input } from 'react-native-elements';
import { regStyles } from '../assets/styles/appStyles';
import constraints from '../validator/constraints';
import md5 from 'md5';
import axios from 'axios';

export default class HomePage extends Component {
    static navigationOptions = { header: null }
    
    constructor (props) {
        super(props)
        this.state = {
            visible:false,
            data: {
                email: '',
                password: '',
                cpassword: '',
            },
        }
        this._registerCheckHandler = this._registerCheckHandler.bind(this);
        this._onRegisterButton = this._onRegisterButton.bind(this);
    }

    _registerCheckHandler() {
        const { data } = this.state

        const validationResult = validate(data, constraints);
        this.setState({...this.state, errors: validationResult })
        
        if(JSON.stringify(validationResult) == undefined && data.password === data.cpassword) {
            this.setState({visible: true })
        } else {
            this.setState({visible: false })
        }
    }

    _onRegisterButton = async() => {
        const { data } = this.state
        let payload = {
            emailID: data.email,
            password: md5(data.password)
        }

        const validationResult = validate(this.state.data, constraints);
        this.setState({ errors: validationResult })

        if(JSON.stringify(validationResult) == undefined && data.password === data.cpassword) {

            try {
                const res = await axios.post(`http://10.0.3.2:3000/regUser`, payload)
                const resJson = await res.data

                if(resJson.status == 'success') {
                    alert('Congratulation, your account successfully registered!')
                    this.props.navigation.navigate('Profile',{
                        emailID: data.email
                    })
                }
            } catch (err) {
                alert('Sorry, email already taken!')
            }
        }else{
            alert('Sorry, your information is mismatched.')
        }
    }

    render () {
        const { visible } = this.state
        return (
            <>
            <View style={regStyles.container}>
                <ScrollView>
                <View style={regStyles.inputGroup}>
                    <Text style={regStyles.regHeader}>SIGN UP</Text>
                    
                    {/* Mail Input */}
                    <Input
                        containerStyle={{marginBottom: 20}}
                        inputContainerStyle={{marginHorizontal: 10}}
                        placeholder='Email ID'
                        keyboardType='email-address'
                        leftIcon={<Icon name='mail' size={24} color='black' style={{left: -13}} />}
                        onChangeText={(email) => (
                            this.setState({...this.state,
                                data : {...this.state.data, email: email}
                            })
                        )}
                        onEndEditing={this._registerCheckHandler}
                        value={this.state.data.email}
                        autoCapitalize='none'
                    />

                    {/* Password Input */}
                    <Input
                        containerStyle={{marginBottom: 20}}
                        inputContainerStyle={{marginHorizontal: 10}}
                        placeholder='Password'
                        secureTextEntry={true}
                        leftIcon={<Icon name='lock' size={24} color='black' style={{left: -13}} />}
                        onChangeText={(password) => (
                            this.setState({...this.state,
                                data : { ...this.state.data, password: password }
                            })
                        )}
                        onEndEditing={this._registerCheckHandler}
                        value={this.state.data.password}
                        autoCapitalize='none'
                    />

                    {/* Confirmation password */}
                    <Input
                        containerStyle={{marginBottom: 20}}
                        inputContainerStyle={{marginHorizontal: 10}}
                        placeholder='Re-Enter Password'
                        secureTextEntry={true}
                        leftIcon={<Icon name='lock' size={24} color='black' style={{left: -13}} />}
                        onChangeText={(cpassword) => (
                            this.setState({...this.state,
                                data : { ...this.state.data, cpassword: cpassword }
                            })
                        )}
                        onEndEditing={this._registerCheckHandler}
                        value={this.state.data.cpassword}
                        autoCapitalize='none'
                    />

                    { this.getErrorMessages() ? 
                        <Text style={{color: 'red', fontSize:12, textAlign:'center'}}>
                            {this.getErrorMessages()}
                        </Text> :
                        <View />
                    }

                    { visible &&
                        <TouchableOpacity  
                        style={regStyles.btnSubmit}
                        onPress={this._onRegisterButton}>
                            <Text>REGISTER</Text>
                        </TouchableOpacity>
                    }

                </View>                
                </ScrollView>
            </View>
            
            
            <View style={{backgroundColor:'#000'}}>
                <TouchableOpacity  style={regStyles.loginFooter}
                    onPress={() => this.props.navigation.navigate('Login')}
                >
                    <Text style={regStyles.loginTxt}>Already registered? Login Here</Text>
                </TouchableOpacity>
            </View>
        </>
        )
    }

    getErrorMessages(separator="\n") {
        const { errors } = this.state;
        if (!errors) return [];
        return Object.values(errors).map(it => it.join(separator)).join(separator);
    }
    
    getErrorsInField(field) {
        const { errors } = this.state;
        return errors && errors[field] || [];
    }

    isFieldInError(field) {
        const { errors } = this.state;
        return errors && !!errors[field];
    }

}