import { StyleSheet } from 'react-native'

export const homeStyles = StyleSheet.create({
    topContainer: {
        flex: 4, 
        backgroundColor: '#ffd800',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btmContainer : {
        flex: 2, 
        justifyContent: 'center',
        alignItems:'center', 
        backgroundColor:'#ffd800'
    },
    btnContainer: {
        borderWidth:1, 
        width: '80%', 
        justifyContent: 'center', 
        alignItems:'center',
        paddingVertical:20, 
        marginBottom: 20,
        borderRadius: 50
    },
    btnTxt: {
        fontSize: 16,
        letterSpacing: 3
    }
})

export const regStyles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#ffd800',
    },
    regHeader: {
        alignSelf: 'center', 
        marginBottom: 30, 
        fontSize: 38, 
        fontFamily:'sans-serif-light'
    },
    inputGroup: {
        margin: 30, 
        marginTop: 100,
        height: 'auto',
        paddingVertical: 30,
        justifyContent: 'center'
    },
    txtInput: {
        borderBottomColor: '#000', 
        borderBottomWidth: .5, 
        marginHorizontal: 15,
        marginVertical: 10,
        fontSize: 18,
        fontFamily: 'sans-serif-light'
    },
    btnSubmit: {
        marginTop: 20,
        borderWidth:1,
        borderRadius:30,
        width: '50%', 
        alignSelf:'center', 
        alignItems:'center', 
        justifyContent:'center', 
        width:'60%',
        height:50
    },
    loginFooter: {
        height: 80, 
        justifyContent: 'center', 
        alignItems:'center', 
        backgroundColor:'#000'
    },
    loginTxt:{
        color:'#ffd800', 
        fontSize: 16, 
        fontFamily: 'sans-serif-light'
    }
})

export const profStyles = StyleSheet.create({
    header: {
        height: 55, 
        backgroundColor:'#ffd800', 
        elevation:5, 
        flexDirection:'row'
    },
    imgContainer: {
        alignSelf: 'center', 
        alignItems:'center', 
        justifyContent: 'center', 
        marginVertical: 20, 
        borderRadius:75, 
        width: 150, height: 150,
    }
})