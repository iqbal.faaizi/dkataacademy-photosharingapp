import React from 'react'
import {View, Text, ActivityIndicator } from 'react-native'
import { Image } from 'react-native-elements'
import { Avatar } from 'react-native-paper';
import AntIcon from 'react-native-vector-icons/AntDesign'

export const PhotoList = (item) => {
    const username = item.emailID.substr(0, item.emailID.length-19)
    return(
        <View>
            <View style={{flexDirection: 'row', marginVertical:15, alignItems:'center'}}>
                <Avatar.Image size={38} source={{uri : `http://10.0.3.2:3000/getDp/${item.emailID}`}} />
                <Text style={{marginLeft:10, fontSize: 16, fontWeight: 'bold'}}>
                    {username}
                </Text>
            </View>
            <View style={{left: -18}}>
                <Image
                    source={{ uri: `http://10.0.3.2:3000/getPost/${item.post_path.substr(10)}` }}
                    style={{ width: '110%', height: 400}}
                    PlaceholderContent={<ActivityIndicator />}
                />
            </View>
        </View>
    )
}

export const FeedList = (item) => {
    const username = item.emailID.substr(0, item.emailID.length-19)
    return(
        <>
        <View>
            <View style={{flexDirection: "row", marginVertical: 10}}>
                <AntIcon name='like2' size={32} style={{marginRight: 20}} />
                <AntIcon name='message1' size={32} />
            </View>
            <Text style={{fontSize: 16}}>
                <Text style={{fontWeight:'bold'}}>{username} </Text>{item.caption}
            </Text>
        </View>
        </>
    )
}
