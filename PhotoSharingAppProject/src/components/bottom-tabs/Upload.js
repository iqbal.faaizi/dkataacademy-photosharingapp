import React, {Component} from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import { TextInput } from 'react-native-paper';
import PhotoUpload from 'react-native-photo-upload';
import { Card, Divider, Header } from 'react-native-elements'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'
import { postData } from '../../actions/ActionCreator'
import {profStyles} from '../../assets/styles/appStyles'

class Upload extends Component {
    static navigationOptions = {header: null}
    constructor(props){
        super(props);
        this.state = {
            caption: '',
            photoData: '',
            email: '',
        }
    }

    async componentDidMount() {
        const emailStore = await AsyncStorage.getItem('email')
        this.setState({email: emailStore})
    }

    _onPressSave = async () => {
        const { postData, data, navigation } = this.props
        const { caption, photoData, email } = this.state
        let payload = {
            caption,
            photoData
        }
        try {
            await postData(`http://10.0.3.2:3000/newPost/${email}`,payload)
            if(data){
                setTimeout(() => {
                    alert(data.message)
                    navigation.navigate('Feeds')
                },1000)
            }
        } catch (err) {
            console.error(err)
        }
    }
    
    render() {
        return (
        <>
            <Header
                leftComponent={
                    <TouchableOpacity style={{marginRight: 10}}>
                        <Text>Discard</Text>
                    </TouchableOpacity>
                }
                centerComponent={{ text: 'POST A PHOTO', style: { color: '#000' } }}
                rightComponent={
                    <TouchableOpacity style={{marginRight: 10}}
                        onPress={() => this._onPressSave()}
                    >
                        <Text>Save</Text>
                    </TouchableOpacity>
                }
                containerStyle={{
                    backgroundColor: '#ffd800',
                    justifyContent: 'space-around',
                }}
            />

            <ScrollView style={{backgroundColor: '#e2e2e2'}}>
            <Card style={{flex: 1}}>
                <PhotoUpload
                    onPhotoSelect={avatar => {
                        if (avatar) {
                            this.setState({ photoData: avatar })
                        }
                    }}
                >
                    <Image
                        style={{
                            paddingVertical: 30,
                            width: 350,
                            height: 350,
                            borderRadius: 75,
                        }}
                        resizeMode='cover'
                        source={{uri: 'https://www.islesburghdramagroup.com/wp-content/uploads/2015/07/Image-Edition-Tools-Screenshot-icon.png'}}
                    />
                </PhotoUpload>

                <Divider style={{ backgroundColor: '#000', marginVertical: 20 }} />

                <TextInput
                    multiline={true}
                    label='Captions'
                    value={this.state.caption}
                    onChangeText={text => this.setState({ caption: text })}
                />
            </Card>
            </ScrollView>
        </>
        )
    }
}

const mapStateToProps = state => ({
    data : state.apiReducer.data
})

const mapDispatchToProps = dispatch => ({
    postData: (url, payload) => dispatch(postData(url, payload))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Upload)