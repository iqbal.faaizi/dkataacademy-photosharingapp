import React, {Component} from 'react'
import {
    View, Text,
    Image,
    ActivityIndicator
} from 'react-native'
import { Card, Header } from 'react-native-elements'
import { Avatar } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5'
import { connect } from 'react-redux'
import { getUserData } from '../../actions/ActionCreator'
import AsyncStorage from '@react-native-community/async-storage'

class UserProfile extends Component {
    static navigationOptions = {header: null}

    constructor(props) {
        super(props);
        this.state = {
          email: ''
        }
    }

    async componentDidMount() {
        const { getUserData } = this.props
        const emailStore = await AsyncStorage.getItem('email')
        this.setState({ email: emailStore })
        
        if(emailStore) {
            await getUserData(`http://10.0.3.2:3000/getUser/${this.state.email}`)
        } else {
            this.props.navigation.navigate('HomePage')
        }
    }

    render() {
        const { userData } = this.props
        return (
        <>
            <Header
                leftComponent={{ text: userData.username, style: { color: '#000', fontSize: 20, fontWeight: 'bold' } }}
                rightComponent={{ icon: 'menu', type:'anticon', color: '#000' }}
                containerStyle={{
                    backgroundColor: '#ffd800',
                    justifyContent: 'space-around',
                }}
            />
            
            <View style={{flex: 1, backgroundColor:'#e2e2e2'}}>
               
                <Card>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{flex:2, padding:10}}>
                            {
                                userData.dp_path ?
                                <Avatar.Image size={130} source={{uri : `http://10.0.3.2:3000/getDp/${userData.emailID}`}} />
                                :
                                <Avatar.Icon size={130} icon="alpha" />
                            }
                   
                        </View>
                        <View style={{flex: 4, marginLeft: 20, justifyContent:'center', alignItems: 'center', flexDirection:'row'}}>
                            <View style={{alignItems:"center"}}>
                                <Text style={{fontWeight:'bold', fontSize: 22}}>3</Text>
                                <Text>Posts</Text>
                            </View>
                            <View style={{alignItems:"center", marginHorizontal:35}}>
                                <Text style={{fontWeight:'bold', fontSize: 22}}>3</Text>
                                <Text>Followers</Text>
                            </View>
                            <View style={{alignItems:"center"}}>
                                <Text style={{fontWeight:'bold', fontSize: 22}}>3</Text>
                                <Text>Following</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{marginTop: 15}}>
                        <Text style={{fontWeight:'bold'}}>{userData.fullname}</Text>
                        <Text>{userData.bio}</Text>
                    </View>
                </Card>
                
                <Card>
                    <Text>Photo</Text>
                </Card>
            </View>
        </>
        )
    }
}

const mapStateToProps = state => ({
    userData: state.apiReducer.userData,
    email: state.apiReducer.email
});

const mapDispatchToProps = dispatch => ({
    getUserData: url => dispatch(getUserData(url))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(UserProfile);