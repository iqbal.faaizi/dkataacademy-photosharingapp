import React, {Component} from 'react'
import {
    View, Text, 
    FlatList,
} from 'react-native'
import { Header, ListItem, Tile } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { getData } from '../../actions/ActionCreator'
import { FeedList, PhotoList } from './FeedList'
import { ActivityIndicator } from 'react-native-paper'

class Feed extends Component {
    static navigationOptions = {header: null}
    constructor (props) {
        super(props);
        this.state = {
            email : '',
        }
    }

    async componentDidMount() {
        const { getData } = this.props
        const emailStore = await AsyncStorage.getItem('email')
        this.setState({email: emailStore})
        
        if(emailStore) {
            await getData(`http://10.0.3.2:3000/getFeeds/${this.state.email}`)
        } else {
            this.props.navigation.navigate('HomePage')
        }
    }

    keyExtractor = (item, index) => index.toString()

    renderItem = ({ item }) => (
        <ListItem
            title={PhotoList(item)}
            subtitle={FeedList(item)}
            bottomDivider
        />
    )

    render() {
        const {data} = this.props
        
        return (
        <>
        <Header
            leftComponent={{ text: 'Feeds', style: { color: '#000', fontSize: 20 } }}
            rightComponent={{ icon: 'mail', type:'anticon', color: '#000' }}
            containerStyle={{
                backgroundColor: '#ffd800',
                justifyContent: 'space-around',
            }}
        />
        {
            data ?
            <FlatList
                keyExtractor={this.keyExtractor}
                data={data}
                renderItem={this.renderItem}
            /> :
            <ActivityIndicator />
        }
        </>
        )
    }
}

const mapStateToProps = state => ({
    email: state.apiReducer.email,
    data: state.apiReducer.data,
});

const mapDispatchToProps = dispatch => ({
    getData: url => dispatch(getData(url))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Feed);