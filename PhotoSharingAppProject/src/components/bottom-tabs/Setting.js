import React, {Component} from 'react'
import { View, Text, ScrollView } from 'react-native'
import {ListItem, Icon, Button, SearchBar } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage'

const logout = [
    {
        title: 'Logout',
        icon: 'log-out',
        ScreenName: 'Home',
    },
]

export default class Setting extends Component {
  static navigationOptions ={
    title : 'Setting',
    headerStyle: { backgroundColor: '#ffd800' },
    headerTintColor: '#000',
  }
    constructor(props){
        super(props);
        this.state = {
          email:'',
        }
    }

    _onPressLogout() {
        const {navigation} = this.props
        AsyncStorage.setItem('email', this.state.email)
        setTimeout( ()=> {
          navigation.navigate('HomePage');
          alert('You have successfully logged out')
        },1000 );
    }

    render() {
        return(
          <>
            <ScrollView>
                {
                  logout.map((item, i) => (
                  <ListItem
                    //containerStyle={{backgroundColor: this.state.color}}
                    key={i}
                    title={item.title}
                    titleStyle={{fontWeight: 'bold'}}
                    leftIcon={{ name: item.icon, type: 'feather'}}
                    bottomDivider
                    button
                    onPress={() => this._onPressLogout()}
                    rightElement={<Icon containerStyle={{ alignSelf: 'flex-start' }} type="font-awesome" color="#C8C8C8" name="sign-out" />}
                  />
                  ))
              }
            </ScrollView>
            </>
        )
    }
}