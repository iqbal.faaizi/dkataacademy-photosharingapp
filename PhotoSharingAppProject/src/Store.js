import { createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import rootReducer from './reducers/photoReducer'

const middleware = [thunk]
if (__DEV__) {
    middleware.push(createLogger())
}

const Store = createStore(
    rootReducer,
    compose(applyMiddleware(...middleware))
)

export default Store