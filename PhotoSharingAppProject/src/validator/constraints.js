export const constraints = {
  email: {
    presence: {
      allowEmpty: false,
      message: "^Please enter an email address"
    },
    email: {
      message: "^Please enter a valid email address"
    },
    format: {
        pattern: /^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+.)?[a-zA-Z]+.)?(realuniversity).edu$/,
        message: "^Please enter university email address"
    }
  },
  password: {
      presence: {
          allowEmpty: false,
          message: "^Please enter a password"
      },
      length: {
          minimum: 6,
          message: "must be at least 6 characters"
      }
  },
};

export default constraints;