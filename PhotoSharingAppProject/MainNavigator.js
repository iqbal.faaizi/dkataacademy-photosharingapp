import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import HomePage from './src/screens/HomePage'
import Register from './src/screens/Register'
import Login from './src/screens/Login'
import Profile from './src/screens/Profile'
import ContentNavigator from './src/components/ContentNavigator'
import { connect } from 'react-redux'

class ContentNav extends React.Component {
    static navigationOptions = { header: null }
    render() {
        return (
        <>
            <ContentNavigator />
        </>
        )
    }
}

export const MainNavigator = createStackNavigator({
    HomePage: {screen: HomePage},
    Register: {screen: Register},
    Login: {screen: Login},
    Profile: {screen: Profile},
    Content: {screen: ContentNav},
},{initialRouteName: 'HomePage'})

export default createAppContainer(MainNavigator)