'use strict'

const Hapi = require('@hapi/hapi')
const Qs = require('qs')
const { userRoutes } = require('./src/routes/userRoutes')
const { postRoutes } = require('./src/routes/postRoutes')
const { validate } = require('./src/configs/users-validate')
const Path = require('path')

const start = async () => {
    const server = Hapi.server({
        port: 3000,
        host: '0.0.0.0',
        query: {
            parser: (query) => Qs.parse(query)
        },
        routes : {
            files: {
                relativeTo: Path.join(__dirname, 'uploads')
            }
        }
    })

    try {
        await server.register(require('@hapi/basic'))
        await server.register(require('@hapi/inert'))
        await server.register({
            plugin: require('hapi-pino'),
            options: {
                prettyPrint: process.env.NODE_ENV !== 'production',
                stream: './logs/logs.log',
                redact: ['req.headers.authorization']
            }
        })

        server.auth.strategy('simple', 'basic', {validate})
        server.route(userRoutes)
        server.route(postRoutes)

        await server.start()
        console.log(`Server running at: ${server.info.uri}`)
        return server
    } catch (err) {
        server.log('err', 'An error occured');
        process.exit(1)
    }
}

start()