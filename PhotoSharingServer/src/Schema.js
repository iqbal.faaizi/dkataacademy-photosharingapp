const mongoose = require('mongoose')
const mongoUrl = 'mongodb://localhost/photoSharingDB'
mongoose.connect(mongoUrl,{ 
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
mongoose.set('debug', true)

const Schema = mongoose.Schema

const userSchema = new Schema ({
    id: Number,
	emailID: {type: String, unique: true},
    password: String,
    dp_path: String,
	fullname: String,
    username: String,
    bio: String,
    address: String,
	phone: String,
	gender: String,
    born_date: Date,
    reg_date: Date,
    active: Boolean
})

const postSchema = new Schema ({
    emailID: String,
    post_id: Number,
    post_path: String,
    caption: String,
    post_date: Date,
    like: Number,
    comments: [
        {
            username: String,
            comment: String,
            comment_date: Date
        }
    ]
})

const Users = mongoose.model('users', userSchema)
const Posts = mongoose.model('posts', postSchema)

module.exports = { Users, Posts }