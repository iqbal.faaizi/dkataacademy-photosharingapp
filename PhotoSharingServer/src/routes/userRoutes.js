'use strict'

const Joi = require('@hapi/joi')
const { userHandlers } = require('./handlers/UserHandlers')

const userRoutes =  [
    {
        method: 'GET',
        path: '/',
        handler: async(req, h) => { 
            return h.response({message: 'OK'}).code(200) 
        }
    },
    {
        method: 'GET',
        path: '/getUser/{email}',
        handler: userHandlers.getByEmail
    },
    {
        method: 'POST',
        path: '/regUser',
        handler: userHandlers.regUser
    },
    {
        method: 'PUT',
        path: '/updProfile/{email}',
        handler: userHandlers.updProfile,
    },
    {
        method: 'POST',
        path: '/user/login',
        handler: userHandlers.userLogin,
    },
    {
        method: 'GET',
        path: '/getDp/{email}',
        handler: {
            file : function (req) {
                return `${req.params.email}/dp/display_pic.jpg`
            }
        }
    },
]

module.exports = { userRoutes }