const fs = require('fs');
const { Users } = require('../../Schema');
const btoa = require('btoa');

const userHandlers = {
    getByEmail: async (req, h) => {
        try {
            const user = await Users.findOne({emailID : req.params.email})
            console.log(user)
            return h.response(user).code(200)
        } catch (err) {
            return h.response(err).code(400)
        }
    },
    regUser: async (req, h) => {
        try {
            const emailCheck = await Users.findOne({"emailID": req.payload.emailID})

            if (emailCheck === null) {
                const gUser = await Users.aggregate([{
                    $group: {
                        _id : "max",
                        maxId: { $max: '$id' }
                    }
                }])

                //console.log(req.payload)

                let nextId = gUser.length > 0 ? parseInt(gUser[0].maxId) + 1 : 1
                //let nextId = parseInt(gUser[0].maxId) + 1
                Object.assign( req.payload, { id: nextId, reg_date:new Date(), active: true })
                
                await Users.insertMany(req.payload)
    
                return h.response({ status: 'success'}).code(201)
            } else {
                return h.response({ status: 'failed'}).code(400)
            }
        } catch (err) {
            return h.response(err).code(400)
        }
    },

    updProfile: async (req, h) => {
        const email = req.params.email
        //console.log("reached handler")
        //const userCheck = await Users.findOne({"username": req.payload.username})
        try {
            const userPayload = req.payload.userData
            let timestamp = new Date().getTime();
            let dir = `./uploads/${email}/dp/`
            let fileName = `display_pic.jpg`
            let dp_path = dir+fileName

            const body = Object.assign(userPayload, {dp_path: dp_path})
            await Users.findOneAndUpdate({"emailID":email}, body, {new: true}).lean()

            // create directory for images if not exist
            !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true })
            
            // create image to server using Image Buffer
            fs.writeFile(dp_path, req.payload.photoData, 'base64', function(err) {
                if(err) {
                    throw new Error(err)
                }
            });

            let result = await Users.findOne({ emailID: email }).lean()
            return h.response(result).code(202)
        } catch (err) {
            throw new Error(err)
        }

    },

    userLogin: async (req, h) => {
        const user = await Users.findOne({"emailID": req.payload.emailID}).lean()

        if(user !== null) {
            if(req.payload.password === user.password) {
                let key = btoa(`${user.emailID}:${user.password}`)
                return h.response({auth:true, type: "Basic", key: key}).code(201)
            } else {
                return h.response({auth: false})
            }
        } else {
            return h.response({auth: false})
        }
    }
}

module.exports = { userHandlers }