const fs = require('fs');
const { Users, Posts } = require('../../Schema');

const postHandlers = {
    newPost: async (req, h) => {
        const email = await Users.findOne({'emailID': req.params.email}).lean()
        const photoStr = req.payload.photoData
        
        if(req.params.email == email.emailID){
            try{
                let timestamp = new Date().getTime();
                let dir = `./uploads/${email.emailID}/post/`
                let fileName = `${timestamp}.jpg`
                let post_path = dir+fileName

                const payload = {
                    emailID: req.params.email,
                    post_path: post_path,
                    caption: req.payload.caption,
                    post_date: new Date(),
                }
                
                await Posts.insertMany([payload])
                
                // create directory for images if not exist
                !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true })
                
                fs.writeFile(post_path, photoStr, 'base64', function(err) {
                    if(err) {
                        throw new Error(err)
                    }
                });

                return h.response({message: 'Your photo has been posted'}).code(201)
            } catch (err) {
                return h.response(err).code(400)
            }
        } else {
            return h.response({error: 'An error occured.'})
        }
    },
    getFeeds: async (req, h) => {
        try {
            const email = req.params.email
            const post = await Posts.find({emailID: email}).lean()
            return h.response(post).code(200)
        } catch(err) {
            return h.response(err).code(400)
        }
    }
}

module.exports = { postHandlers }