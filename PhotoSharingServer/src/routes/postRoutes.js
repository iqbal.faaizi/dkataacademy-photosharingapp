'use strict'

const Joi = require('@hapi/joi')
const { userHandlers } = require('./handlers/UserHandlers')
const { postHandlers } = require('./handlers/PostHandlers')

const postRoutes =  [
    {
        method: 'GET',
        path: '/getFeeds/{email}',
        handler: postHandlers.getFeeds
    },
    {
        method: 'POST',
        path: '/newPost/{email}',
        handler: postHandlers.newPost
    },
    {
        method: 'GET',
        path: '/getPost/{email}/post/{filename}',
        handler: (req, h) => {
            return h.file(`${req.params.email}/post/${req.params.filename}`)
        },
    },
]

module.exports = { postRoutes }