# Photo Sharing App
Photo Sharing App is a mobile based application that can share photos with your campus friends.

### Quick Start
Install [PhotoSharingApp](http://https://gitlab.com/iqbalalfaaizi/dkataacademy-photosharingapp "PhotoSharingApp"), or just use it directly from master:
#### Install Server
```
git clone https://gitlab.com/iqbalalfaaizi/dkataacademy-photosharingapp.git
cd PhotoSharingServer
```

Install node_modules using npm
```
npm install
```

Run using node or nodemon
`node Server.js` or `nodemon Server.js`

#### Install RN App
```
cd PhotoSharingAppProject
```

Install node_modules library using npm
```
npm install
```

Run RN App
`react-native run-android` or `react-native start`